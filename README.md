# s6-rc - a dependency-based init script management system
--------------------------------------------------------

 s6-rc is a suite of programs designed to help Unix distributions manage
services provided by various software packages, and automate initialization,
shutdown, and more generally changes to the machine state.

 It keeps track of the complete service dependency tree and automatically
brings services up or down to reach the desired state.

 In conjunction with s6, it ensures that long-lived services are
supervised, and that short-lived instructions are run in a reproducible
manner.

 See https://skarnet.org/software/s6-rc/ for details.


## Installation
  ------------

 See the INSTALL file.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-rc/s6-rc.git && 
cd s6-rc.git && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

```
sudo apt install git-buildpackage skalibs-dev libexecline-dev libs6-dev s6
```

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

These flags will need to be added to the `debian/rules` file.



* Contact information
  -------------------

 Laurent Bercot <ska-skaware at skarnet.org>

 Please use the <skaware at list.skarnet.org> mailing-list for
questions about the inner workings of s6, and the
<supervision at list.skarnet.org> mailing-list for questions
about process supervision, init systems, and so on.
